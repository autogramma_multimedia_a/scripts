#!/bin/bash
# Meant to be called by install_android.sh
# Added GPT feature for Android 7.1.2_r9

LOG_STDOUT="/opt/install_stdout.log"
LOG_STDERR="/opt/install_stderr.log"

# partition size in MB
BOOTLOAD_RESERVE=8
BOOT_ROM_SIZE=32
SYSTEM_ROM_SIZE=1536
CACHE_SIZE=512
RECOVERY_ROM_SIZE=32
DEVICE_SIZE=8
MISC_SIZE=4
DATAFOOTER_SIZE=2
METADATA_SIZE=2
FBMISC_SIZE=1
PRESISTDATA_SIZE=1

. /usr/bin/echos.sh

help() {
	bn=`basename $0`
	echo " usage $bn <option> device_node"
	echo
	echo " options:"
	echo " -h			displays this help message"
	echo " -f soc_name		flash android image."
	echo " -u			flash uboot only"
}

# Parse command line
moreoptions=1
node="na"
soc_name=""
uboot_name=""
cal_only=0
uboot_only=0

while [ "$moreoptions" = 1 -a $# -gt 0 ]; do
	case $1 in
	    -h) help; exit ;;
	    -f) soc_name=$2; shift;;
	    -u) uboot_only=1; ;;
	    *)  moreoptions=0; block=$1; is_dart=$2; ;;
	esac
	[ "$moreoptions" = 0 ] && [ $# -gt 2 ] && help && exit
	[ "$moreoptions" = 1 ] && shift
done

systemimage_file="system_raw.img"
bootimage_file="boot-${soc_name}.img"
recoveryimage_file="recovery-${soc_name}.img"

if [[ $soc_name == "ag-mx6q" ]]; then
    uboot_name="ag-mx6q"
else
    uboot_name="var-imx6"
fi

spl_file_nand="SPL-nand"
spl_file_emmc="SPL-emmc"

uboot_file_nand="u-boot-${uboot_name}-nand.img"
uboot_file_emmc="u-boot-${uboot_name}-emmc.img"
imagesdir=/opt/images/Android
node=/dev/${block}
part=""
if [[ $block == mmcblk* ]] ; then
	part="p"
fi

echo "================================================" >> $LOG_STDERR
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDERR
echo "================================================" >> $LOG_STDERR
echo "================================================" >> $LOG_STDOUT
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDOUT
echo "================================================" >> $LOG_STDOUT

function die
{
	red_bold_echo "ОШИБКА: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации"
	exit 1
}

function die_warn
{
	red_bold_echo "ПРЕДУПРЕЖДЕНИЕ: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации"
}

function check_image()
{
	if [[ ! -f ${imagesdir}/${1} ]] ; then
		red_bold_echo "ОШИБКА: ${1} не найден! Убедитесь, что данные скопированы правильно и SD карта не повреждена"
		exit 1
	fi
	
	md5sum -c ${imagesdir}/${1}.md5sum < ${imagesdir}/${1} > /dev/null
	if [ $? -ne 0 ]; then
		red_bold_echo "ОШИБКА: ${1} возможно повреждён! Убедитесь, что данные скопированы правильно и SD карта не повреждена"
		exit 1
	fi
}

function check_images
{
	blue_underlined_bold_echo "ШАГ 1: Проверка целостности файлов"
	if [[ ! -b $node ]] ; then
		red_bold_echo "ОШИБКА: \"$node\" не блочное устройство"
		exit 1
	fi
	
	check_image ${spl_file_nand}
	check_image ${spl_file_emmc}
	check_image ${uboot_file_nand}
	check_image ${uboot_file_emmc}
	check_image ${bootimage_file}
	check_image ${recoveryimage_file}
	check_image ${systemimage_file}
}

function delete_device
{
	blue_underlined_bold_echo "ШАГ 2: Удаление текущих разделов"
	for ((i=0; i<=12; i++))
	do
		if [[ -e ${node}${part}${i} ]] ; then
			dd if=/dev/zero of=${node}${part}${i} bs=1024 count=1024 >> $LOG_STDOUT 2>> $LOG_STDERR || true
		fi
	done
	sync

	((echo d; echo 1; echo d; echo 2; echo d; echo 3; echo d; echo w) | fdisk $node >> $LOG_STDOUT 2>> $LOG_STDERR) || true
	sync


	fdisk -u -l ${node}

	dd if=/dev/zero of=$node bs=1M count=4 >> $LOG_STDOUT 2>> $LOG_STDERR || true
	sync; sleep 1
}

function create_parts
{
	# call sfdisk to create partition table
	# get total card size
	seprate=100
	total_size=`sfdisk -s ${node}`
	total_size=`expr ${total_size} \/ 1024`
	boot_rom_sizeb=`expr ${BOOT_ROM_SIZE} + ${BOOTLOAD_RESERVE}`
	extend_size=`expr ${SYSTEM_ROM_SIZE} + ${CACHE_SIZE} + ${DEVICE_SIZE} + ${MISC_SIZE} + ${FBMISC_SIZE} + ${PRESISTDATA_SIZE} + ${DATAFOOTER_SIZE} + ${METADATA_SIZE} +  ${seprate}`
	data_size=`expr ${total_size} - ${boot_rom_sizeb} - ${RECOVERY_ROM_SIZE} - ${extend_size}`

	blue_underlined_bold_echo "ШАГ 3: Создание Android разделов (GPT)"
	
	# Echo partitions
	echo -e "TOTAL       : ${total_size}MB
U-BOOT      : ${BOOTLOAD_RESERVE}MB
BOOT        : ${BOOT_ROM_SIZE}MB
RECOVERY    : ${RECOVERY_ROM_SIZE}MB
SYSTEM      : ${SYSTEM_ROM_SIZE}MB
CACHE       : ${CACHE_SIZE}MB
DATA        : ${data_size}MB
MISC        : ${MISC_SIZE}MB
DEVICE      : ${DEVICE_SIZE}MB
DATAFOOTER  : ${DATAFOOTER_SIZE}MB
METADATA    : ${METADATA_SIZE}MB
FBMISC      : ${FBMISC_SIZE}MB
PRESISTDATA : ${PRESISTDATA_SIZE}MB" >> $LOG_STDOUT 2>> $LOG_STDERR

	echo
	
	blue_underlined_bold_echo "Creating Android partitions"
	SECT_SIZE_bytes=`cat /sys/block/${block}/queue/hw_sector_size`

	boot_rom_sizeb_sect=`expr $boot_rom_sizeb \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	BOOTLOAD_RESERVE_sect=`expr $BOOTLOAD_RESERVE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`

	RECOVERY_ROM_SIZE_sect=`expr $RECOVERY_ROM_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`

	extend_size_sect=`expr $extend_size \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	SYSTEM_ROM_SIZE_sect=`expr $SYSTEM_ROM_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	CACHE_SIZE_sect=`expr $CACHE_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	DEVICE_SIZE_sect=`expr $DEVICE_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	MISC_SIZE_sect=`expr $MISC_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	DATAFOOTER_SIZE_sect=`expr $DATAFOOTER_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	METADATA_SIZE_sect=`expr $METADATA_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	FBMISC_SIZE_sect=`expr $FBMISC_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`
	PRESISTDATA_SIZE_sect=`expr $PRESISTDATA_SIZE \* 1024 \* 1024 \/ $SECT_SIZE_bytes`

sfdisk --force -uS ${node} &> /dev/null << EOF
,${boot_rom_sizeb_sect},83
,${RECOVERY_ROM_SIZE_sect},83
,${extend_size_sect},5
,-,83
,${SYSTEM_ROM_SIZE_sect},83
,${CACHE_SIZE_sect},83
,${DEVICE_SIZE_sect},83
,${MISC_SIZE_sect},83
,${DATAFOOTER_SIZE_sect},83
,${METADATA_SIZE_sect},83
,${FBMISC_SIZE_sect},83
,${PRESISTDATA_SIZE_sect},83
EOF

	sync; sleep 3

	# Adjust the partition reserve for the bootloader
	((echo d; echo 1; echo n; echo p; echo $BOOTLOAD_RESERVE_sect; echo; echo w) | fdisk -u $node &> /dev/null)

	sync; sleep 3

	fdisk -u -l $node
}

function install_bootloader
{
	blue_underlined_bold_echo "ШАГ 4: Установка загрузчика(NAND+EMMc)"
	
	fdisk -u -l $node >> $LOG_STDOUT 2>> $LOG_STDERR || die
	blue_underlined_bold_echo " ... : Очистка NAND"
	flash_erase /dev/mtd0 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR
	flash_erase /dev/mtd1 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR
	
	blue_underlined_bold_echo " ... : Установка NAND загрузчика(Для совместимости с Varisite)"
	
	kobs-ng init -x ${imagesdir}/${spl_file_nand} --search_exponent=1 -v >> $LOG_STDOUT 2>> $LOG_STDERR
	nandwrite -p /dev/mtd1 ${imagesdir}/${uboot_file_nand} >> $LOG_STDOUT 2>> $LOG_STDERR

	blue_underlined_bold_echo " ... : Устаноlвка EMMc загрузчика(Для работы на IM6x-BOARD)"

	dd if=${imagesdir}/${spl_file_emmc} of=$node bs=512 seek=2 conv=fsync ; sync

	dd if=${imagesdir}/${uboot_file_emmc} of=$node bs=512 seek=138 conv=fsync; sync

}

function format_android
{
	fdisk -u -l $node >> $LOG_STDOUT 2>> $LOG_STDERR || die
	umount ${node}${part}* >> $LOG_STDOUT 2>> $LOG_STDERR || true
	blue_underlined_bold_echo "ШАГ 5: Подготовка разделов для Android"
	mkfs.ext4 -F ${node}${part}4 -L data >> $LOG_STDOUT 2>> $LOG_STDERR || die
	mkfs.ext4 -F ${node}${part}5 -Lsystem >> $LOG_STDOUT 2>> $LOG_STDERR || die
	mkfs.ext4 -F ${node}${part}6 -Lcache  >> $LOG_STDOUT 2>> $LOG_STDERR || die
	mkfs.ext4 -F ${node}${part}7 -Ldevice >> $LOG_STDOUT 2>> $LOG_STDERR || die
}

function install_android
{
	blue_underlined_bold_echo "ШАГ 6: Копирование файлов"
	
	blue_underlined_bold_echo " ... : Установка boot образа Android: $bootimage_file"
	dd if=${imagesdir}/${bootimage_file} of=${node}${part}1 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	blue_underlined_bold_echo " ... : Установка recovery образа Android: $recoveryimage_file"
	dd if=${imagesdir}/${recoveryimage_file} of=${node}${part}2 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	blue_underlined_bold_echo " ... : Установка system образа Android: $systemimage_file"
	dd if=${imagesdir}/$systemimage_file of=${node}${part}5 bs=1M conv=fsync >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync; sleep 1
}

check_images

if [[ $uboot_only == "0" ]]; then
	umount ${node}${part}* >> $LOG_STDOUT 2>> $LOG_STDERR || true
	/etc/init.d/udev stop >> $LOG_STDOUT 2>> $LOG_STDERR #Stop Udev for block devices while partitioning in progress
	delete_device
	create_parts
	sleep 3
	for i in `cat /proc/mounts | grep "${node}" | awk '{print $2}'`; do umount $i >> $LOG_STDOUT 2>> $LOG_STDERR; done
	hdparm -z ${node} >> $LOG_STDOUT 2>> $LOG_STDERR
fi

install_bootloader

if [[ $uboot_only == "0" ]]; then
	format_android
	install_android

	/etc/init.d/udev start >> $LOG_STDOUT 2>> $LOG_STDERR #Start Udev back before exit
fi

exit 0
