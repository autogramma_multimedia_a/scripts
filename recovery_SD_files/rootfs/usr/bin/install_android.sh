#!/bin/sh

# Installs Android
clear

echo "Установка Android начнется в течении 20 секунд"
echo "Чтобы отменить, войдите в консоль под root(пароль пустой) и выполните:"
echo " touch /tmp/.no-flash"

sleep 20s

. /usr/bin/echos.sh

usage()
{
	echo
	echo "This script installs Andorid on the SOM's internal storage device/s"
	echo
	echo " Usage: $0 OPTIONS"
	echo
	echo " OPTIONS:"
	echo " -u			Install u-boot only"
	echo
}

blue_underlined_bold_echo "*** Autogramma Android eMMC/NAND Recovery ***"
set +e
blue_bold_echo "Версия: $(red_bold_echo $(cat /opt/images/Android/githash-mark))"

if [ -f /tmp/.no-flash ]; then
	red_bold_echo "Прошивка отменена"
	exit 1
fi

block=/dev/mmcblk2

die()
{
	sync
	echo
	red_bold_echo "УСТАНОВКА ANDROID ЗАВЕРШЕНА С ОШИБКАМИ"
	exit 1
}
set -e
cd /opt/images/Android
var-mksdcard.sh -f imx8mp-var-som-symphony $block || die

sync
echo
blue_bold_echo "УСТАНОВКА ANDROID ЗАВЕРШЕНА"

init 1 > /dev/null # Prepare to poweroff, shutdown daemons
mount -o remount,ro /dev/root / > /dev/null # Remount to RO
blue_bold_echo "ПИТАНИЕ МОНИТОРА МОЖНО ОТКЛЮЧИТЬ"
exit 0
