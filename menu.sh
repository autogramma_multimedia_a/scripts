#!/bin/bash

#################################################################
#                                                               #
# Скрипт позволяет автоматизировать часто повторяющиеся задачи. #
# Все сценарии в данном файле рассчитаны на сборку Android для  #
# устройства МК монитора.                                       #
#                                                               #
#################################################################

items=() # Массив ключей и текстов для пунктов меню.
nodes=() # Массив блочных устройств и их размеров.

# Обновляет массив nodes.
nodes_array_update() {
	nodes=( $(lsblk | grep -E 'sd[a-z] ' | awk '{ print $1, "[" $4 "]" }') )
}

# Возвращает последнее блочное устройство, доступное в системе.
get_last_node() {
	nodes_array_update
	echo ${nodes[${#nodes[@]}-2]}
}

# item_add <тег> <текст_пункта_меню>
# Добавляет новый элемент в в меню.
item_add() {
	local menu_index=$1
	local title=$2

	local index=$(( $menu_index * 2 ))
	items[$index]=$1
	items[$index+1]="$title"
}

# item_title_by_index <тег_пункта_меню>
# Возвращает текст пункта меню по его тегу.
item_title_by_index() {
	echo ${items[$1*2+1]}
}

# Отображает меню доступных сценариев.
# Возвращает тег выбранного пользователем  пункта меню.
show_menu() {
	# Количество пунктов меню в массиве.
	# В массиве записаны последовательно индекс элемент - текст - индекс...,
	#	поэтому размер массива делим пополам.
	local count=$(( ${#items[@]} / 2 ))

	# Список элементов массива.
	local items_list="${items[@]}"

	local title="Устройство: ${TARGET_PRODUCT} | Вариант сборки: ${TARGET_BUILD_VARIANT}"
	local cols=79
	local rows=$(( $count + 7 ))
	
	dialog --title "$title" --default-item $LAST_ITEM --menu "Выберите один из пунктов:" $rows $cols $count "${items[@]}" 2>&1 >/dev/tty
}

# Инициализирует переменные для сборки и позволяет выбрать нужное устройство, для
# которого будет собираться Android.
script_setup_build_env() {
	source build/envsetup.sh && lunch dart_mx8mp-userdebug
}

# Проверка, имеет ли предыдущая сборка поддержку WiFi. Возвращает 1, если да, иначе 0.
script_is_wifi_supported() {
	# android.hardware.wifi.xml обязателен, если устройство действительно поддерживает WiFi :)
	if [ -e "./out/target/product/dart_mx8mp/system/etc/permissions/android.hardware.wifi.xml" ]; then
		echo "1"
	else
		echo "0"
	fi
}

# Очищает бинарные файлы сборки для дальнейшей пересборки.
# Этот этап нужен как гарантия того, что все важные образы будут пересобраны.
# Выполнение этого шага рекомендовано документацией Variscite.
# Первый аргумент опциональный, если он равен wifi, то используется конфиг для wifi версии
script_clean_binaries_for_rebuild() {
	local bootloader_file="u-boot-imx8mp-var-dart.imx"
	local bootimage_file="boot.img"
	local vendor_bootimage_file="vendor_boot.img"
	local systemimage_file="system.img"
	local vendorimage_file="vendor.img"
	local productimage_file="product.img"
	local superimage_file="super.img"

	if [ -z "${OUT}" ]; then
		echo " Целевая директрия не задана, очистка невозможна."
		return 1
	fi

	echo "*******************************************************************************"
	echo " Очищаем бинарные файлы сборки для дальнейшей пересборки "
	echo " Удаление Device Tree Blobs в директории kernel_imx..."
	rm -vf kernel_imx/arch/arm/boot/dts/*.dtb
	
	echo " Удаление временных файлов в директории kernel_imx..."
	rm -vf kernel_imx/.config
	rm -vf kernel_imx/.missing-syscalls.d
	rm -vf kernel_imx/.tmp_kallsyms1.o
	rm -vf kernel_imx/.tmp_kallsyms2.o
	rm -vf kernel_imx/.tmp_System.map
	rm -vf kernel_imx/.tmp_vmlinux2
	rm -vf kernel_imx/.version
	rm -vf kernel_imx/.vmlinux.cmd

	echo " Очистка данных в директории: ${OUT}"

	echo " Удаление данных bootloader'а..."
	rm -vf ${OUT}/boot*.img
	rm -vf ${OUT}/*.dtb
	rm -vf ${OUT}/dtb*.img
	rm -vf ${OUT}/vbmeta*.img
	rm -vrf ${OUT}/root

	echo  "Удаление данных восстановления..."
	rm -vf ${OUT}/recovery*.img
	rm -vrf ${OUT}/recovery

	echo " Удаление recovery_SD образов..."
	rm -vrf ${OUT}/${bootloader_file}
	rm -vrf ${OUT}/${bootimage_file}
	rm -vrf ${OUT}/${vendor_bootimage_file}
	rm -vrf ${OUT}/${systemimage_file}
	rm -vrf ${OUT}/${productimage_file}
	rm -vrf ${OUT}/${superimage_file}

	echo " Завершено."
	echo "*******************************************************************************"
	echo ""

	return 0
}

# Перезапуск устройств через fastboot
script_fastboot_reboot() {
	echo "*******************************************************************************"
	echo " fastboot команда для перезагрузки "
	echo "*******************************************************************************"
	fastboot reboot
	echo ""
}

# Вход в загрузчик через fastboot reboot-bootloader
script_reboot_bootloader() {

	local DEV_LIST=`fastboot devices`
	if [[ !  -z  $DEV_LIST  ]]
	then 
		echo "*******************************************************************************"
		echo " Устройство уже в загрузчике "
		echo "*******************************************************************************"
		echo ""
	else
		echo "*******************************************************************************"
		echo " Попытка перезагрузится в загрузчик (можно просто перезагрузить девайс)"
		echo "*******************************************************************************"
		adb wait-for-device
		sleep 1
		adb reboot-bootloader
		echo ""
	fi
}

# Спрашивает пользователя о необходимости перезапустить устройство со входом в загрузчик
# Результат сохраняется в переменную REBOOT_BOOTLOADER (1 - перезапуск, 0 - пропустить).
ask_for_reboot_bootloader() {
	if [ -z "${REBOOT_BOOTLOADER}" ]; then REBOOT_BOOTLOADER="1"; fi
	if [ "${REBOOT_BOOTLOADER}" -eq "1" ]; then
		QUESTION_VARIANTS="Y/n"
	else
		QUESTION_VARIANTS="y/N"
	fi
	echo "*******************************************************************************"
	read -t 10 -p " Зайти в загрузчик через ADB? [${QUESTION_VARIANTS}]? (10 сек)> " yn
	if [ "$?" -eq "0" ]; then
		case $yn in
			[YyДд]* )
				REBOOT_BOOTLOADER="1"
				;;
			[NnНн]* )
				REBOOT_BOOTLOADER="0"
				;;
		esac
	fi
	if [ "${REBOOT_BOOTLOADER}" -eq "1" ]; 
	then
		echo "*******************************************************************************"
		echo " "
	else
		echo " Перезагрузка в загрузчик не производится"
		echo "*******************************************************************************"
		echo " "
	fi

	if [ "${REBOOT_BOOTLOADER}" -eq "1" ]; 
	then
		script_reboot_bootloader
	fi
}

# Прошивка дистрибутива Autogramma Variscite Android 11.
script_fastboot_autogramma() {
	local soc_name="imx8mp-var-som-symphony"
	local dtboimage_file="dtbo-${soc_name}.img"
	local bootloader_file="u-boot-imx8mp-var-dart.imx"
	local bootimage_file="boot.img"
	local vendor_bootimage_file="vendor_boot.img"
	local systemimage_file="system.img"
	local vendorimage_file="vendor.img"
	local productimage_file="product.img"
	local superimage_file="super.img"
	local vbmeta_file="vbmeta-${soc_name}.img"

	echo "*******************************************************************************"
	echo " Попытка прошить образы через fastboot flash из:"
	echo " ${OUT}"
	echo ""
	echo " Если загрузчик заблокирован можно разлочить его так:"
	echo " Settings => System => About Tablet => Build number."
	echo " Keep on tapping until you see a prompt that says (You are now a developer!)"
	echo " Settings => System => Advanced => Deveoper options => OEM unlocking."
	echo " reboot to bootloader via."
	echo ""
	echo " Если нет udev правила его можно добавить так:"
	echo " echo 'SUBSYSTEMS=="usb", ATTRS{idVendor}=="0525", ATTRS{idProduct}=="a4a5", GROUP="users", MODE="0666"' | sudo tee -a /etc/udev/rules.d/51-fastboot.rules"
	echo "*******************************************************************************"
	echo ""

	if [ -z "${OUT}" ]; then
		echo " Целевая директрия не задана, найти образы невозможно."
		return 1
	fi

	echo " Разблокируем загрузчик (первый раз занимает 30 сек времени):"
	fastboot oem unlock
	echo; echo "*******************************************************************************"
	echo " Прошиваем:"

	fastboot flash dtbo_a ${OUT}/${dtboimage_file} #1
	fastboot flash dtbo_b ${OUT}/${dtboimage_file} #2
	fastboot flash boot_a ${OUT}/${bootimage_file} #3
	fastboot flash boot_b ${OUT}/${bootimage_file} #4
	fastboot flash vendor_boot_a ${OUT}/${vendor_bootimage_file} #5
	fastboot flash vendor_boot_b ${OUT}/${vendor_bootimage_file} #6
	#7 misc
	#8 metadata
	#9 presistdata
	fastboot flash super ${OUT}/${superimage_file} #10
	#11 userdata
	#12 fbmisc
	fastboot flash vbmeta_a ${OUT}/${vbmeta_file} #13
	fastboot flash vbmeta_b ${OUT}/${vbmeta_file} #14

	echo "*******************************************************************************"
	echo ""
}

script_is_wifi_supported() {
	# android.hardware.wifi.xml обязателен, если устройство действительно поддерживает WiFi :)
	if [ -e "$ANDROID_PRODUCT_OUT/system/etc/permissions/android.hardware.wifi.xml" ] || [ -e "out/target/product/dart_mx8mp/system/etc/permissions/android.hardware.wifi.xml" ]; then
		echo "1"
	fi
}

script_make_recovery_SD() {
	local SPL_DIR=device/variscite/common/firmware
	local DISTR_DIR=out/target/product/dart_mx8mp

	mkdir -pv ${DISTR_DIR}/recovery_SD
	sudo ./copy_img.sh $DISTR_DIR $DISTR_DIR/recovery_SD ag-mx6q ag-mx6q $(script_is_wifi_supported)

	echo "*******************************************************************************"
	echo " Копируем содержимое: ${DISTR_DIR}/recovery_SD "
	echo " на SD Card  в папку: /opt/images/Android/"
	echo "*******************************************************************************"
}

# Сборка дистрибутива для emmc
script_make_emmc() {
	local DISTR_DIR=out/target/product/dart_mx8mp
	local CPU_NUM=$(grep -c ^processor /proc/cpuinfo)
	
	local product_makefile=`pwd`/`find device/variscite -maxdepth 4 -name "${TARGET_PRODUCT}.mk"`;
	local product_path=${product_makefile%/*}
	local soc_path=${product_path%/*}
	local nxp_git_path=${soc_path%/*}
	export AARCH64_GCC_CROSS_COMPILE=$ANDROID_BUILD_TOP/prebuilts/gcc/linux-x86/aarch64/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/bin/aarch64-linux-gnu-
	export CLANG_PATH=$ANDROID_BUILD_TOP/prebuilts/clang/host/linux-x86
	
	local parallel_option="-j${CPU_NUM}"
	local build_bootloader="bootloader"
	local build_kernel="${OUT}/kernel"
	local build_vvcam="vvcam"
	local build_galcore=""
	local build_mxmwifi="mxmwifi"
	
	local build_bootimage=""
	local build_vendorbootimage=""
	local build_dtboimage=""
	local build_vendorimage=""
	
	
	echo "*******************************************************************************"
	echo " Переменные окружения: "
	echo " TARGET_PRODUCT: ${TARGET_PRODUCT}"
	echo " product_makefile: ${product_makefile}"
	echo " product_path: ${product_path}"
	echo " soc_path: ${soc_path}"
	echo " nxp_git_path: ${nxp_git_path}"
	echo " AARCH64_GCC_CROSS_COMPILE: ${AARCH64_GCC_CROSS_COMPILE}"
	echo " CLANG_PATH: ${CLANG_PATH}"
	echo " OUT: ${OUT}"
	echo "*******************************************************************************"
	
	echo; echo "*******************************************************************************"
	echo " Собираем ядро и загрузчик"
	echo; echo
	soc_path=${soc_path} \
	product_path=${product_path} \
	nxp_git_path=${nxp_git_path} \
	clean_build=${clean_build} \
	make -C ./ -f ${nxp_git_path}/../nxp/common/build/Makefile \
	${parallel_option} ${build_bootloader} ${build_kernel} \
	</dev/null || exit
	echo "*******************************************************************************"

	echo; echo "*******************************************************************************"
	echo " Собираем внешние для ядра модули"
	echo; echo
	soc_path=${soc_path} \
	product_path=${product_path} \
	nxp_git_path=${nxp_git_path} \
	clean_build=${clean_build} \
	make -C ./ -f ${nxp_git_path}/../nxp/common/build/Makefile ${parallel_option} \
	${build_vvcam} ${build_galcore} ${build_mxmwifi} \
	</dev/null || exit
	echo "*******************************************************************************"
	
	
	echo; echo "*******************************************************************************"
	echo " Собираем образы андройда"
	echo; echo
	make ${parallel_option} \
	${build_bootimage} \
	${build_vendorbootimage} \
	${build_dtboimage} \
	${build_vendorimage}
	echo "*******************************************************************************"
	echo
	
	echo " Конветируем system.img"
	echo; echo "*******************************************************************************"
	simg2img ${DISTR_DIR}/super.img ${DISTR_DIR}/super_raw.img
}

menu_item_1_selected() {
	echo "*******************************************************************************"
	echo " Для успешной сборки потребуется не менее 200 Gb"
	echo " Пакеты для сборки андройда:"
	echo " sudo apt-get -y install git-core gnupg flex bison gperf build-essential zip curl\\"
	echo " zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev\\"
	echo " libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip mtd-utils \\"
	echo " u-boot-tools lzop liblzo2-2 liblzo2-dev zlib1g-dev liblz-dev uuid uuid-dev libz-dev gdisk\\"
	echo " android-tools-fsutils bc dialog liblz4-tool repo dos2unix android-tools-adb libssl-dev m4\\"
	echo " android-tools-fastboot kpartx device-tree-compiler"
	echo " Так-же надо выполнить команды:"
	echo " git config --global user.email \"Ваша_почта@mail.com\""
	echo " git config --global user.name \"Ваше Имя\""
	echo " sudo apt-get update"
	echo " sudo apt-get install openjdk-8-jdk"
	echo " sudo add-apt-repository ppa:openjdk-r/ppa"
	echo " sudo update-alternatives --config java"
	echo " sudo update-alternatives --config javac"
	echo " "
	echo "*******************************************************************************"
	echo " Пакеты для сборки ёкты:"
	echo " sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib\\"
	echo " build-essential chrpath socat cpio python python3 python3-pip python3-pexpect\\"
	echo " xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa\\"
	echo " libsdl1.2-dev pylint3 xterm"
	echo " "
	echo "*******************************************************************************"
	echo " Для корректнй работы ADB и Fastboot надо сделать парвило udev так"
	echo " sudo touch /etc/udev/rules.d/10-local.rules"
	echo " sudo nano /etc/udev/rules.d/10-local.rules"
	echo " В открывшийся файл текст:"
	echo " ACTION==\"add\", SUBSYSTEM==\"usb\", ATTR{idVendor}==\"18d1\", ATTR{idProduct}==\"4ee7\", MODE=\"0666\", GROUP=\"plugdev\", SYMLINK+=\"android_adb\", OWNER=\"modelkiniy\""
	echo " ACTION==\"add\", SUBSYSTEM==\"usb\", ATTR{idVendor}==\"18d1\", ATTR{idProduct}==\"0d02\", MODE=\"0666\", GROUP=\"plugdev\", SYMLINK+=\"android_adb\", OWNER=\"modelkiniy\""
	echo " OWNER=\"modelkiniy\" - заметиь на имя своего пользователя"
	echo " "
	echo "*******************************************************************************"
	echo " Терминал: адекватно не собтрается из исходников и пакетов."
	echo " Просто берём его из prebuilts/kermit/"
	echo " Кладём себе в /bin/..."
        echo " "
	echo "*******************************************************************************"
        echo " Сборка на виртуалке!"
        echo " Собирается без проблем но возможно вам не удастся пробросить в вируалку ADB и "
        echo " fastboot usb устройства. Выход в том что вы пользуете ADB и fastboot "
        echo " из хост машины забирая из виртуалки готовые образы через общую папку."
        echo " Ставите доп софт вируалки. Общую папку ставить маунтится в папку:"
        echo " ..\Android_7_1_2_M\out\target\product\vm_out"
        echo " "
        echo "*******************************************************************************"
        echo " Сборка тестовых полных DTS и DTB"
        echo " cd vendor/variscite/kernel_imx/arch/arm64/boot/dts/freescale/"
        echo " cpp -nostdinc -I ../../../../../include/ -undef -x assembler-with-cpp imx8mp-var-som-symphony.dts test_full.dts"
        echo " dtc -I dts -O dtb -p 0x1000 test_full.dts -o test_full.dtb"
        echo " "
        echo "*******************************************************************************"
	          	
}

menu_item_2_selected() {
	echo "*******************************************************************************"
	echo " Синхронизация Древа исходных кодов"
	echo " Если вы перемешали репозитори или меняли манифест repo"
	echo " Запустите в ручную комадой:"
	echo " repo sync -vj3 --force-sync --fail-fast"
	echo "*******************************************************************************"
	repo sync -vj3 --fail-fast
}

menu_item_3_selected() {
	local DEV_CONNECT=$(adb devices | grep -w "device")

	if [ "$?" -eq "0" ]
	then
		echo "*******************************************************************************"
		echo " Есть подключенный девайс:($DEV_CONNECT)"
		read -t 10 -p " прошить после сборки? [y/N](10 сек)> " yn
		
		REBOOT_BOOTLOADER="0"
		
		if [ "$?" -eq "0" ]; then
			case $yn in
				[YyДд]* )
					REBOOT_BOOTLOADER="1"
					;;
			esac
		fi
	fi

	if [ "$REBOOT_BOOTLOADER" -eq "1" ]
	then
		echo ""
		echo " После сборки попытаемся перепрошить через fastboot"
		echo "*******************************************************************************"
		echo ""
		sleep 1
		script_clean_binaries_for_rebuild wifi && script_make_emmc && script_reboot_bootloader && script_fastboot_autogramma && script_fastboot_reboot
	else
		echo ""
		echo " Просто собираем андройд"
		echo "*******************************************************************************"
		echo ""
		sleep 1
		script_clean_binaries_for_rebuild wifi && script_make_emmc
	fi
}

menu_item_4_selected() {
	ask_for_reboot_bootloader 
	script_fastboot_autogramma && script_fastboot_reboot
}

menu_item_5_selected() {

	local START_PATH=$(pwd)

        echo "*******************************************************************************"
	echo " Собираем дистрибутив LRT"
	echo "*******************************************************************************"
	echo ""

	export MACHINE=imx8mp-var-dart
	export DISTRO=fsl-imx-wayland

	pushd lrt
	source var-setup-release.sh -b build_wayland
	bitbake ag-image-lrt

	if [ "$?" -eq "0" ]
	then
		popd
		echo "*******************************************************************************"
		echo " Готово! Создаём ссымвольную ссылку на собранный LRT"
		echo " $START_PATH/out/yocto_deploy"
		echo "*******************************************************************************"
		echo ""

		rm $START_PATH/out/yocto_deploy

		ln -s $START_PATH/lrt/build_wayland/tmp/deploy/images/imx8mp-var-dart/ $START_PATH/out/yocto_deploy
	else
		echo "*******************************************************************************"
		echo " Сборка завершилась провалом, устраните проблему и попробуйте снова."
		echo " Переменные окружения LRT:"
		echo "  $ source var-setup-release.sh -b lrt/build_wayland"
		echo " Пересборка LRT:"
		echo "  $ bitbake fsl-image-gui"
		echo "*******************************************************************************"
		echo ""
	fi
	cd $START_PATH
}

menu_item_6_selected() {
	if [ -z "${OUT}" ]; then
		echo " Целевая директрия не задана, найти образы невозможно."
		return 1
	fi

	local soc_name="imx8mp-var-som-symphony"

	./yocto_lrt_iso_creator.sh ${OUT} ${soc_name}
}

menu_item_7_selected() {
	script_make_recovery_SD
}

menu_item_8_selected() {
	echo "*******************************************************************************"
	echo " Ожидаю первое устройство ADB"
	echo "*******************************************************************************"
	echo ""
	adb wait-for-device
	adb shell
}

menu_item_9_selected() {
	adb shell mount -o rw,remount /system
}

menu_item_10_selected() {

	if [ -z "${OUT}" ]; then
		echo " Целевая директрия не задана, очистка невозможна."
		return 1
	fi

	echo "*******************************************************************************"
	echo " Очищаем бинарные файлы сборки для дальнейшей пересборки "

	echo "rm -rf ${OUT}/system"
	rm -vrf ${OUT}/system

	echo "rm -rf ${OUT}/root"
	rm -vrf ${OUT}/root

	echo "rm -rf ${OUT}/obj/APPS"
	rm -vrf ${OUT}/obj/APPS
	
	echo " Удаление временных файлов в директории ${OUT}/obj/KERNEL_OBJ"
	rm -vrf ${OUT}/obj/KERNEL_OBJ

	echo " Удаление временных файлов в директории ${OUT}/obj/UBOOT_OBJ"
	rm -vrf ${OUT}/obj/UBOOT_OBJ

	echo " Удаление временных файлов в директории ${OUT}/obj/UBOOT_COLLECTION"
	rm -vrf ${OUT}/obj/UBOOT_COLLECTION

	echo " Удаление всех файлов в корне ${OUT}"
	rm -vf ${OUT}/*.*

	echo " Завершено."
	echo "*******************************************************************************"
	echo ""

	return 0
}

menu_item_11_selected() {

       local DISTR_DIR=out/target/product/dart_mx8mp
       local DISTR_LRT_DIR=out/lrt_tmp
       local OUT_DIR=out/target/product/vm_out

       if [ -d "$OUT_DIR" ]; then

         echo "*******************************************************************************"
         echo " Экспорт в расшаренную папку виртуалки"
         echo "*******************************************************************************"
         rm -vf ${OUT_DIR}/*
         cp -v ${DISTR_DIR}/boot-ag-mx6q.img  ${OUT_DIR}/boot-ag-mx6q.img 
         cp -v ${DISTR_DIR}/recovery-ag-mx6q.img  ${OUT_DIR}/recovery-ag-mx6q.img 
         cp -v ${DISTR_DIR}/system.img  ${OUT_DIR}/system.img
         touch ${OUT_DIR}/_fastboot.bat
         echo "adb.exe -d wait-for-device" >> ${OUT_DIR}/_fastboot.bat
         echo "timeout 3 /NOBREAK" >> ${OUT_DIR}/_fastboot.bat
         echo "adb.exe -d shell setprop monitor.otg.mode force_device" >> ${OUT_DIR}/_fastboot.bat
         echo "adb.exe -d reboot bootloader" >> ${OUT_DIR}/_fastboot.bat
         echo "timeout 1" >> ${OUT_DIR}/_fastboot.bat
         echo "fastboot.exe flash boot ./out/boot-ag-mx6q.img" >> ${OUT_DIR}/_fastboot.bat
         echo "fastboot.exe flash recovery ./out/recovery-ag-mx6q.img" >> ${OUT_DIR}/_fastboot.bat
         echo "fastboot.exe flash system  ./out/system.img" >> ${OUT_DIR}/_fastboot.bat
         echo "fastboot.exe reboot" >> ${OUT_DIR}/_fastboot.bat
         echo "adb.exe -d wait-for-device" >> ${OUT_DIR}/_fastboot.bat
         echo "timeout 3 /NOBREAK" >> ${OUT_DIR}/_fastboot.bat
         echo "adb.exe -d shell setprop monitor.otg.mode force_device" >> ${OUT_DIR}/_fastboot.bat
         
        if [ -d "$DISTR_LRT_DIR" ]; then
          echo "*******************************************************************************"
          echo " Есть образ SD карты его тоже экспортируем."
          echo "*******************************************************************************"
          cp -v ${DISTR_LRT_DIR}/out.iso  ${OUT_DIR}/out.iso
        fi
       else
         echo "*******************************************************************************"
         echo " Папка:${OUT_DIR} не сушествует экспорт отменён"
         echo "*******************************************************************************"
         echo ""
         return
       fi
       
}

menu_item_q_selected() {
	:
}

# Точка входа в сценарий.
main() {
	# Если переменные окружения не были настроены, то настраиваем.
	if [ -z "${TARGET_PRODUCT}" ]; then script_setup_build_env; fi

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM="0"; fi
	if [ -z "${LAST_NODE}" ]; then LAST_NODE="$(get_last_node)"; fi

	item_add 1 "FAQ На всякий если что-то забыл/незнал как делать"
	item_add 2 "REPO: синхронизация дерева исходных кодов"
	item_add 3 "ANDRIOD: Сборка дистрибутива"
	item_add 4 "ANDRIOD: Прошивка через fastboot"
	item_add 5 "LRT: Сборка дистрибутива"
	item_add 6 "LRT: Создание образа SD Card с текущими собраными LRT и ANDRIOD"
	item_add 7 "-LRT: Обновление на готовом LRT только образов текущего ANDRIOD"
	item_add 8 "Service: Поймать ADB консоль после перезагрузки."
	item_add 9 "Service: Перемонтировать раздел /system в режиме чтения-записи"
	item_add 10 "Service: Очистка собранных образов/ядра/apk (10 мин сборка)"
	item_add 11 "-Service: Экспорт образов для вируалки ./vm_out..."
	
	
	item_add q "Выйти"
	LAST_ITEM=$(show_menu)

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM='q'; fi
	clear
	menu_item_${LAST_ITEM}_selected
}

main
