#!/bin/bash
set -e

blue_underlined_bold_echo()
{
	echo -e "\e[34m\e[4m\e[1m$@\e[0m"
}

die()
{
	STATUS=$?
	echo $1
	exit $STATUS
}

#Пути параметры временных файлов
TMPFOLDER="out/lrt_tmp"
TMPFILE="out.iso"
#Параметры второго раздела
ROOTFS_NAME=ag-image-lrt-imx8mp-var-dart.tar.gz
UBOOT_NAME=imx-boot
DTB_NAME=imx8mp-var-som-symphony.dtb
KERNEL_NAME=Image.gz
YOCTO_DEPLOY_LINK=out/yocto_deploy

soc_name=$2
imagesdir=$1
bootloader_offset=32
#bootloader_file="u-boot-imx8mp-var-dart.imx"
bootloader_file="u-boot.imx"
dtboimage_file="dtbo-${soc_name}.img"
bootimage_file="boot.img"
vendor_bootimage_file="vendor_boot.img"
vbmeta_file="vbmeta-${soc_name}.img"
systemimage_file="system.img"
vendorimage_file="vendor.img"
productimage_file="product.img"
superimage_file="super.img"

if [[ $UID != "0" ]]; then
	echo " "
	echo "*******************************************************************************"
	echo "Необходимы привелигии администратора для работы скрипта"
	echo "Перезапускаем себя от администратора"
	echo "*******************************************************************************"
	echo " "
	# Имя текущего скрипта
	SCRIPTNAME="$(realpath -s $0)"
	exec sudo $SCRIPTNAME $@
fi

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 0: чистим временные файлы/папки"

echo " по пути $TMPFOLDER/"
rm -rdf $TMPFOLDER || die

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 1: Подcчёт объёма образов Android"

SRC_SIZE=0
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${bootloader_file} | grep -oE "\b[0-9]+" ) `
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${dtboimage_file} | grep -oE "\b[0-9]+" ) `
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${bootimage_file} | grep -oE "\b[0-9]+" ) `
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${vendor_bootimage_file} | grep -oE "\b[0-9]+" ) `
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${superimage_file} | grep -oE "\b[0-9]+" ) `
SRC_SIZE=` expr ${SRC_SIZE} + $( du -L --block-size=1 ${imagesdir}/${vbmeta_file} | grep -oE "\b[0-9]+" ) `

echo " Образы Android занимют на файловой системе `expr ${SRC_SIZE}`  Байт"

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 2: LRT Подcчёт объёма образа"

ROOTFS_ST_OFSET=`expr 8193 \* 512`
ROOTFS_FREE_SPACE=`expr 1000 \* 1024 \* 1024`
ROOTFS_SIZE=( $(tar -tvzf $YOCTO_DEPLOY_LINK/$ROOTFS_NAME |awk '{ s += $3 } END { print s }') )

echo " Образы LRT занимют на файловой системе $ROOTFS_SIZE байт"

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 3: LRT Создаём файл заготовку"

TMPFILESIZE=`expr ${ROOTFS_ST_OFSET} + ${ROOTFS_SIZE} + ${SRC_SIZE} + ${ROOTFS_FREE_SPACE}`

echo " по пути $TMPFOLDER/$TMPFILE размером $TMPFILESIZE Байт"
mkdir $TMPFOLDER -p || die
touch $TMPFOLDER/$TMPFILE || die
chmod -R 777 $TMPFOLDER || die
dd if=/dev/zero of=$TMPFOLDER/$TMPFILE bs=1K count=`expr ${TMPFILESIZE} / 1024 + 1` || die

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 4: LRT Делаём таблицу разделов"

echo; echo " Монтируем образ как loop усройство"
node_1=$(losetup -f --show ./$TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]+")

echo; echo " Временное loop устройство: ${node_1}"
set +e
(echo o; echo w ) | fdisk ${node_1}
set -e

echo; echo " Отключем устройстово ${node_1}"
sync; sleep 3
losetup -v -d ${node_1} || die
sync; sleep 3

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 5: LRT Размечаем разделы"

echo; echo " Монтируем образ как loop усройство"
node_1=$(losetup -f --show ./$TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]+")

echo; echo " Временное loop устройство: ${node_1}"

#8193 16385 b
sfdisk --force -uS ${node_1} << EOF
`expr ${ROOTFS_ST_OFSET} / 512` `expr $( expr ${TMPFILESIZE} - ${ROOTFS_ST_OFSET} ) / 512` 83;
EOF

echo; echo " Отключем устройстово ${node_1}"
sync; sleep 3
losetup -v -d ${node_1} || die
sync; sleep 3

echo; echo "*******************************************************************************"
blue_underlined_bold_echo "Этап 9: LRT Подключем разделы для установки образат rootfs LRT"

echo; echo " Монтируем образ как loop усройство"
node_1=$(losetup -f --show ./$TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]+")

echo; echo " Временное loop устройство: ${node_1}"

echo; echo " Подключаем разделы"
node_1_p=( $(kpartx -av ${node_1} | grep -oE "loop[0-9]+p[0-9]+") )
echo ${node_1_p[0]}
sync; sleep 1

echo; echo " Копируем через dd загрузчик ./$YOCTO_DEPLOY_LINK/$UBOOT_NAME"
dd if=./$YOCTO_DEPLOY_LINK/$UBOOT_NAME of=${node_1} bs=1k seek=${bootloader_offset}
sync

echo; echo " Создаём разделы. rootfs"
mkfs.ext4 -F /dev/mapper/${node_1_p[0]} -Lrootfs || die

echo; echo " Монтируем разделы"
mkdir ./$TMPFOLDER/img_fs -p || die
mount /dev/mapper/${node_1_p[0]} ./$TMPFOLDER/img_fs -t ext4 || die

echo; echo " Распаковываем в rootfs ${ROOTFS_NAME}"
tar -xf ./$YOCTO_DEPLOY_LINK/$ROOTFS_NAME -C ./$TMPFOLDER/img_fs/

echo; echo " на root fs изначально свободно  $(df --output=avail --block-size=1M /dev/mapper/${node_1_p[0]} | grep -oE "\b[0-9]+") МБайт. Увеличиваем раздел до конца устройства."
sync; sleep 3
resize2fs -F /dev/mapper/${node_1_p[0]} > /dev/zero || die
sync;
echo " на root fs теперь свободно  $(df --output=avail --block-size=1M /dev/mapper/${node_1_p[0]} | grep -oE "\b[0-9]+") МБайт"

echo; echo " копируем образы Android и скрипты его установки"
mkdir ./$TMPFOLDER/img_fs/opt/images/Android -p || die
chmod -R 777 $TMPFOLDER/img_fs/opt || die
cp -Rv scripts/recovery_SD_files/rootfs/* ./$TMPFOLDER/img_fs || die

cp -v ${imagesdir}/${bootloader_file} $TMPFOLDER/img_fs/opt/images/Android/${bootloader_file} || die
md5sum < ${imagesdir}/${bootloader_file} > $TMPFOLDER/img_fs/opt/images/Android/${bootloader_file}.md5sum || die

cp -v ${imagesdir}/${dtboimage_file} $TMPFOLDER/img_fs/opt/images/Android/dtbo.img || die
md5sum < ${imagesdir}/${dtboimage_file} > $TMPFOLDER/img_fs/opt/images/Android/dtbo.md5sum || die

cp -v ${imagesdir}/${bootimage_file} $TMPFOLDER/img_fs/opt/images/Android/${bootimage_file} || die
md5sum < ${imagesdir}/${bootimage_file} > $TMPFOLDER/img_fs/opt/images/Android/${bootimage_file}.md5sum || die

cp -v ${imagesdir}/${vendor_bootimage_file} $TMPFOLDER/img_fs/opt/images/Android/${vendor_bootimage_file} || die
md5sum < ${imagesdir}/${vendor_bootimage_file} > $TMPFOLDER/img_fs/opt/images/Android/${vendor_bootimage_file}.md5sum || die

cp -v ${imagesdir}/${superimage_file} $TMPFOLDER/img_fs/opt/images/Android/${superimage_file} || die
md5sum < ${imagesdir}/${superimage_file} > $TMPFOLDER/img_fs/opt/images/Android/${superimage_file}.md5sum || die

cp -v ${imagesdir}/${vbmeta_file} $TMPFOLDER/img_fs/opt/images/Android/vbmeta.img || die
md5sum < ${imagesdir}/${vbmeta_file} > $TMPFOLDER/img_fs/opt/images/Android/vbmeta.md5sum || die
echo; echo " Размонтируем разделы"
sync; sleep 3

set +e # 2 раза потому что надо перебдеть
sync; sleep 3
umount -dl /dev/mapper/${node_1_p[0]}
umount -dl /dev/mapper/${node_1_p[0]}
set -e

echo; echo "Отключем устройстово ${node_1}"
sync; sleep 3
kpartx -dv ${node_1} || die
sync; sleep 3
losetup -v -d ${node_1} || die

echo " "
echo "*******************************************************************************"
blue_underlined_bold_echo "Готово! Краткая справка:"
echo " Образ SD Карты для установки готов."
echo " Далее этот образ можно залить на SD карту командой:"
echo " sudo dd if=./$TMPFOLDER/$TMPFILE of=/dev/... bs=512"
echo " или"
echo " sudo dcfldd if=./$TMPFOLDER/$TMPFILE of=/dev/... bs=512"
echo " "
echo " Для проверки образа можно использовать команды:"
echo " подключаем образ:"
echo "  sudo losetup -f --show ./$TMPFOLDER/$TMPFILE"
echo "  sudo kpartx -av /dev/loop0"
echo "  sudo mount /dev/mapper/loop0p1 ./$TMPFOLDER/img_fs -t ext4"

echo " "
echo " отмаунчиваем, и отключаем образ:"
echo "  sudo umount -dl /dev/mapper/loop0p1"
echo "  sudo kpartx -dv /dev/loop0"
echo "  sudo losetup -v -d  /dev/loop0"
echo " "
echo " По пути ./$YOCTO_DEPLOY_LINK должна быть ссылка на собранные в Yocto образы"
echo " Linux_recovery_tool"
echo " например /home/(user)/var-fslc-yocto/build/tmp/deploy/images/var-som-mx6"
echo " пользуйтесь командой: "
echo "  ln -s [путь LRT] ./out/yocto_deploy"
echo "*******************************************************************************"
echo " "

exit 0
