#!/bin/bash

# set -x

DST=$1

# Проверка, имеет ли предыдущая сборка поддержку WiFi. Возвращает 1, если да, иначе 0.
script_is_wifi_supported() {
	# android.hardware.wifi.xml обязателен, если устройство действительно поддерживает WiFi :)
	if [ -e "$ANDROID_PRODUCT_OUT/system/etc/permissions/android.hardware.wifi.xml" ] || [ -e "out/target/product/var_mx6/system/etc/permissions/android.hardware.wifi.xml" ]; then
		return "1"
	fi
}

if [[ $DST == "" ]]; then
	DST=/media/$USER/rootfs
        echo "WARNING: destination rootfs isn't supplied. Using $DST as rootfs"
fi


if [[ $UID != "0" ]]; then
	echo "NOTE: Restarting myself as root"
	exec sudo ./update-sdcard.sh $DST
fi

SRC=out/target/product/var_mx6
SRC_BOARD=ag-mx6q
DST_BOARD=ag-mx6q

if [[ ! -e $DST ]]; then
	echo "ERROR: can't find $DST"
	exit 1
fi

echo Preparing...
mkdir -p $DST/opt/images/Android/

echo Copying SD install scripts
sudo cp -Rv scripts/recovery_SD_files/rootfs/* $DST

echo Copying main images
exec sudo ./copy_img.sh $SRC $DST/opt/images/Android $SRC_BOARD $DST_BOARD $(script_is_wifi_supported)


echo Syncing
sleep 1s
sync

echo Unmounting $DST

umount $DST
echo "Done!"

exit 0
