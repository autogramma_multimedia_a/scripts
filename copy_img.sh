#!/bin/bash

# set -x

SRC=$1
DST=$2

SRC_BOARD=$3
DST_BOARD=$4

SRC_WIFI_SUP=$5


if [[ $SRC == "" ]]; then
	SRC=out/target/product/var_mx6
        echo "WARNING: source isn't supplied. Using $SRC as source dir"
fi
if [[ $DST == "" ]]; then
	DST=/media/$USER/rootfs
        echo "WARNING: destination isn't supplied. Using $DST as destination dir"
fi

if [[ $SRC_BOARD == "" ]]; then
	SRC_BOARD=ag-mx6q
        echo "WARNING: source board isn't supplied. Using $SRC_BOARD by default"
fi

if [[ $DST_BOARD == "" ]]; then
	DST_BOARD=ag-mx6q
        echo "WARNING: destination board isn't supplied. Using $DST_BOARD by default"
fi


if [[ $UID != "0" ]]; then
	echo "NOTE: Restarting myself as root"
	exec sudo ./copy_img.sh $SRC $DST $SRC_BOARD $DST_BOARD
fi

if [[ ! -e $SRC ]]; then
	echo "ERROR: can't find $SRC"
	exit 1
fi
if [[ ! -e $DST ]]; then
	echo "ERROR: can't find $DST"
	exit 1
fi


cp_and_md5()
{
    md5sum < $1 > $2.md5sum
    cp $1 $2
}

echo Copying boot
cp_and_md5 $SRC/boot-$SRC_BOARD.img $DST/boot-$DST_BOARD.img

echo Copying recovery
cp_and_md5 $SRC/recovery-$SRC_BOARD.img $DST/recovery-$DST_BOARD.img

# echo Copying partition table
# cp_and_md5 -u $SRC/partition-table.img $DST/opt/images/Android/

echo Copying system_raw
cp_and_md5 $SRC/system_raw.img $DST/system_raw.img

echo Copying SPL
cp_and_md5 $SRC/SPL-${SRC_BOARD}-emmc $DST/SPL-emmc
cp_and_md5 $SRC/SPL-${SRC_BOARD}-nand $DST/SPL-nand

echo Copying uboot
cp_and_md5 $SRC/u-boot-${SRC_BOARD}-emmc.img $DST/u-boot-${DST_BOARD}-emmc.img
cp_and_md5 $SRC/u-boot-${SRC_BOARD}-nand.img $DST/u-boot-${DST_BOARD}-nand.img


if [[ $SRC_WIFI_SUP == "" ]]; then
	echo  Remove Wi-Fi mark
	rm -f $DST/wifi-mark
else
	echo  Create Wi-Fi mark
	touch $DST/wifi-mark
fi

git describe --dirty --always > $DST/githash-mark

echo Syncing
sleep 1s
sync
sleep 1s

echo "images copying is done!"

exit 0
